class Monster {
	constructor(id){


		this.node = document.getElementById("monster_"+id);
		this.posY= 100 + Math.random() * mousePosY;
		this.posX= 100+Math.random() * mousePosX ;
		this.node.style.left = this.posX + "px"

		this.node.style.top = this.posY+"px";

		this.angle=getElementAngle(this.posY,this.posY,mousePosX, mousePosY)
		this.speedMax=4 ;//gravity
		this.speedY=1;
		this.speedX=1;
		this.velocity= 0.1; //velocity aacceleration y
	}

	tick(){

		if (this.posY <mousePosY && this.speedY <=this.speedMax){
			this.speedY += this.velocity;
		}
		else {
			this.speedY -= this.velocity;
		}
		if (this.posX <mousePosX  && this.speedX <=this.speedMax){
			this.speedX += this.velocity;
		}
		else {
			this.speedX -= this.velocity;
		}

		this.posX+=this.speedX;
		this.posY+=this.speedY;



		this.node.style.top = this.posY+ "px";
		this.node.style.left = this.posX+ "px";

		rotateElement(this.node,getElementAngle(this.posY,this.posY,mousePosX, mousePosY));
	}

}