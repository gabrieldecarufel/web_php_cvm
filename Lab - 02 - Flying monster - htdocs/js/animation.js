var mousePosX= 0;
var mousePosY = 0;
const MONSTER_COUNT=1;
spriteList=[]
window.onload = function() {
	for (let i = 0; i < MONSTER_COUNT; i++) {
		spriteList.push(new Monster(i,mousePosX,mousePosY));

	}
	document.getElementById("main").onmousemove = function (event) {
		mousePosX = event.pageX;
		mousePosY = event.pageY;


	}

	tick();
}
const tick = () => {
		for (let i = 0; i < spriteList.length; i++) {
			const sprite = spriteList[i];

			sprite.tick();
		}
		 window.requestAnimationFrame(tick);
	}