<?php
	require_once("DAO/Connection.php");

	class UserDAO {

		public static function authenticate($username, $password) {
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT * FROM USERS");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$userInfo = null;

			if ($row = $statement->fetch()) {
				if (password_verify($password, $row["PASSWORD"])) {
					$userInfo = $row;
				}
			}

			return $userInfo;
		}

		public static function addComment($NAME_COMMENT,$COMMENTAIRE) {
			$connection = Connection::getConnection();
			$statement = $connection->prepare("INSERT INTO USERS (NAME_COMMENT,COMMENTAIRE,DATE_COMMENTAIRE) VALUES (?,?,SYSDATE)");
			$statement->bindParam(1, $NAME_COMMENT);
			$statement->bindParam(2, $COMMENTAIRE);
			$statement->execute();
			$statement->commit();
		}
		public static function getComments() {
			$connection = Connection::getConnection();
			$statement = $connection->prepare("SELECT NAME_COMMENT , COMMENTAIRE from USERS");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			if ($row = $statement->fetch()) {
				$comms  =$rows ;
				}



			return $comms;
		}
	}