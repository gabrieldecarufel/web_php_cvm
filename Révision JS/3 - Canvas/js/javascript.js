const spriteList = [];
let ctx = null;
let background = null;
let startDrop = 20;
let ninja = null;

window.onload = () => {

	ctx = document.querySelector("#canvas").getContext("2d");
	ninja = new NinjaGaden();
	tick();
}


tick = () => {
	ctx.clearRect(0,0,400,300);
	ninja.tick();

	if (spacePushed)
	{
		spacePushed = !spacePushed;
		spriteList.push(new NinjaStar(ninja.posX));
	}

	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		let alive = sprite.tick();

		if (!alive){
			spriteList.splice(i,1);
			i--;
		}
	}


	window.requestAnimationFrame(tick);
}