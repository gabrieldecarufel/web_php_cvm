
class NinjaStar {
	constructor(posX){
		this.posX = posX;
		this.posY = 130;
		this.speed = 1;
		this.img = new Image();
		this.img.onload = () => {
			ctx.drawImage(this.img, 0, 0);
		}
		this.img.src = "images/star.png";

	}

	tick() {
		let alive = true;
		this.posX +=this.speed;

		if (this.posX >= 350)
		{
			alive = false;
		}

		ctx.drawImage(this.img, this.posX, this.posY);
		return alive;
	}
}