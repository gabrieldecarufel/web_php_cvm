
class NinjaGaden {
	constructor(){
		this.posX = 50;
		this.posY = 130;
		this.speed = 1;
		this.img = new Image();
		this.img.onload = () => {
			ctx.drawImage(this.img, 0, 0);
		}
		this.img.src = "images/ninja.png";

	}

	tick() {
		if (rightPushed && this.posX <= 310){
			this.posX +=this.speed;
		}
		if (leftPushed && this.posX >= 0){
			this.posX -=this.speed;
		}
		ctx.drawImage(this.img, this.posX, this.posY);
	}
}