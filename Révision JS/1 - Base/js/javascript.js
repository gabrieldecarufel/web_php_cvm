

window.onload = () => {

	$("p").click( function() {
		$(this).fadeTo(500, 0.5, function() {
			$(this).fadeTo(2000, 1);
		})
	});

}

const validateGet = () => {
	let success = true;
	let form = document.forms['formInfo'];

	let node = form['firstName'];
	success = validateName(node, success);

	node = form['lastName'];
	success = validateName(node, success);

	return success;
}

const validateName = (node,success) => {
	const pattern = /^[a-zA-Z]*$/;
	const strToCompare = node.value;

	if (strToCompare.length <= 0 || strToCompare.match(pattern) == null){
		success = false;
	}

	return success;
}