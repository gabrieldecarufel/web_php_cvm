<?php
	session_start();

	// phpc = snippet pour les classes

	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_ADMINISTRATOR = 1;

		private $pageVisibility;
		public $title;
		public function __construct($pageVisibility, $title) {
			$this->pageVisibility = $pageVisibility;
			$this->title=$title;
		}

		public function execute() {
			if (!empty($_GET["logout"])) {
				session_unset();
				session_destroy();
				session_start();
			}

			if (empty($_SESSION["visibility"])) {
				$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
			}

			if ($_SESSION["visibility"] < $this->pageVisibility) {
				header("location:index.php");
				exit;
			}

			// Template method design pattern
			$this->executeAction();
		}

		public function getUsername() {
			return empty($_SESSION["username"]) ? "Invité" : $_SESSION["username"];
		}

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}



		protected abstract function executeAction();
	}