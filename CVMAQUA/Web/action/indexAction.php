<?php
	require_once("action/CommonAction.php");

	class indexAction extends CommonAction {
		public $textIndex="asd ";

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC, "Accueil");

		}

		protected function executeAction() {
			$this->textIndex = file_get_contents('indexText.txt');

		}

	}