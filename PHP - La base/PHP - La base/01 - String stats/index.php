<?php
$sizeMot= 0;
	if (!empty($_GET["searchBox"])){
		$sizeMot=strlen($_GET["searchBox"]);
	}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>String stats</title>
        <meta charset="utf-8" />
		<link href="css/global.css" rel="stylesheet" />
	</head>
	<body>
		<div>
			<div id="analyze-icon">

			</div>

			<?php
				if($sizeMot>0){
					?>
						<div id="analyze-result"> <?= $sizeMot ?></div>
					<?php

				}

			?>

			<div id="analyze-result">
					Entrez une chaîne de caractères et appuyez sur analyser
			</div>
			<form action="index.php" method="get">
				<div id="search-line">
					<div>
						<input type="text" name="searchBox" placeholder="Texte à analyser" />
					</div>
					<div class="btn">
						<button type="submit">Analyser</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>