<?php
	 function hasher($mot , $salt="querty"){

		$val = sha1($mot . $salt); // md5() gribouille

		return $val;
	 }
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Les fonctions</title>
		<meta charset="utf-8" />
	</head>
	<body>

		<h1>Création d'un hash</h1>
		<?= hasher("gabriel") ?>
		<br>
		<?= hasher("gabriel", "De Carufel") ?>
	</body>
</html>
