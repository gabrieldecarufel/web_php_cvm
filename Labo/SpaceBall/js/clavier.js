var rightPushed = false;
var leftPushed = false;
var spacePushed = false;

document.onkeydown = function (e) {
    if (e.which == 65)         leftPushed = true;
    else if (e.which == 68) rightPushed = true;
    else if (e.which == 32) spacePushed = true;
}

document.onkeyup = function (e) {
    if (e.which == 65)         leftPushed = false;
    else if (e.which == 68) rightPushed = false;
    else if (e.which == 32) spacePushed = false;
}
