
class Meduse {
	constructor(speedaccelerator){
		this.columnCount = 6;
		this.rowCount = 1;
		this.refreshDelay = 200; 			// msec
		this.loopColumns = true; 			// or by row?
		this.scale = 1.0;
		this.tiledImage = new TiledImage("images/meduse-spritesheet.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);

		this.tiledImage.changeRow(0);					// One row per animation
		this.tiledImage.changeMinMaxInterval(1, 6); 	// Loop from which column to which column?

		this.posX = 1000;
		this.posY = /*30 +Math.random() **/ 350;
		this.speed = 1 * speedaccelerator;
		this.dim = 32.5 * this.scale;
	}

	tick() {
		this.posX -= this.speed;
		ctx.fillStyle = "blue";
		ctx.fillRect(this.posX - this.dim/2, this.posY - this.dim, this.dim, this.dim)
		this.tiledImage.tick(this.posX, this.posY, ctx);
		let alive = this.posX > 0;
		return alive;
	}

	touchingMe(posXBob, posYBob, dimBob){
		//valeur de sprite est dans le milieu du carré
		let kill = false;

		let topBob = posYBob - dimBob/2;
		let leftBob = posXBob - dimBob/2;

		let topMed = this.posY  - this.dim -this.dim;
		let leftMed = this.posX - this.dim/2;

		if (topBob >= topMed - this.dim && 	//top >= bottom
			topBob - dimBob <= topMed && 	//bottom <= top

			leftBob <= leftMed + this.dim && 	//left <= right
			leftBob + dimBob >= leftMed		//right >= leftt
			)
		{
			kill = true;
		}
		return kill;
	}
}