// =======================================================
// Animation in a canvas
// -------------------------------------------------------
let ctx = null;
const spriteList = [];
let background = null;
let bob = null;
let nbMed = 0;

// You can stack images!
//tiledImage.addImage("images/item-hood-walk.png");
//tiledImage.addImage("images/item-shield-walk.png");

// Other functions :
//   - tiledImage.setFlipped(true);		// Horizontally
//   - tiledImage.setLooped(false);
window.onload = () => {
	bob = new Bob();

	ctx = document.getElementById("canvas").getContext("2d");

	background = new Image();
	background.onload = () => {
		ctx.drawImage(background, 0, 0);
	}
	background.src = "images/background.png";
	playMe();
	tick();
}

function gameOver(){
	console.log("gameOver man!");
	stopMe();
	document.onkeypress = function (e) {
		if (e.which == 10)
		{
			bob = new Bob();
			spriteList.splice(0,spriteList.length);
			playMe();
			tick();
		}
	}
}

function tick() {
	ctx.clearRect(0, 0, 990, 510);
	ctx.drawImage(background, 0, 0);
	bob.tick();

	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		let alive = sprite.tick();
		bob.alive = !sprite.touchingMe(bob.posX, bob.posY, bob.dim);
		if (!bob.alive)
		{
			break;
		}
		console.log("alive :", bob.alive);
		if (!alive){
			spriteList.splice(i,1);
			i--;
			nbMed++;
		}
	}

	if (spriteList.length < 1 ) /*Math.random() < 0.011*/
	{
		let speedaccelerator = 0.001 * nbMed;
		spriteList.push(new Meduse(speedaccelerator));
	}
	if (bob.alive == true)
	{
		window.requestAnimationFrame(tick);
	}
	else{
		gameOver();
	}
}
