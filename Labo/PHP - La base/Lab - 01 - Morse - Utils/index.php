<?php
	require_once("action/indexAction.php");
	require_once("partial/header.php");

	$resultMorse = executeMorse();
	$resultASCII = executeASCII();
?>


	<form action="index.php" method="post">
		<h2>Chaine a convertir en morse</h2>
		<div>
			<textarea name="morse"><?= $resultMorse ?></textarea>
		</div>
		<h2>Chaine a convertir en ASCII</h2>
		<div>
			<textarea name="ASCII"><?= $resultASCII ?></textarea>
		</div>

		<div>
			<button>Convertir</button>
		</div>
	</form>

<?php
	require_once("partial/footer.php");