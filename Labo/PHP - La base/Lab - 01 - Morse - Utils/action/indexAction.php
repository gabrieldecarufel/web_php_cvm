<?php
	require_once("utils/morse-utils.php");
	function executeMorse() {
		if (!empty($_POST["morse"])){
			return encodeMorse($_POST["morse"]);
		}
	}

	function executeASCII() {
		if (!empty($_POST["ASCII"])){
			return executeMorse(encodeMorse($_POST["ASCII"]));
		}
	}