<?php
	session_start();

	function execute() {
		session_unset(); // unset variables
		session_destroy(); // invalide le cookie
		session_start(); // nouvelle session
	}