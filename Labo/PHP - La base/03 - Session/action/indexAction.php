<?php
	session_start();

	function execute() {
		$courriel = null;

		// isset = défini
		if (isset($_POST["courriel"]) && isset($_POST["motDePasse"]) &&
			$_POST["courriel"] === "test@test.com" &&
			$_POST["motDePasse"] === "test") {

			$courriel = $_POST["courriel"];
			$_SESSION["estConnecte"] = true;
		}

		return $courriel;
	}