<?php
	session_start();


	if (empty($_SESSION["liste"]))
	{
		$_SESSION["liste"] = array();
	}

	function execute(){
		if ( !empty($_GET["memo"]) && !empty($_GET["posX"]) && !empty($_GET["posY"]) ){
			$_POSIT = [$_GET["memo"], $_GET["posX"], $_GET["posY"]];
			$_SESSION["liste"][] = $_POSIT;
		}
		return $_SESSION["liste"];
	}
