<?php
	require_once("action/indexAction.php");

	$listPostIt = execute();

	require_once("partial/header.php");
?>

	<form action="index.php" method="get">

		<div id="container">
			<div class="form-label">Qu'avez-vous en tête : </div>
			<div class="form-input"><input type="text" required name="memo" /></div>

			<div class="form-label"> x : </div>
			<div class="form-input"><input type="number" required name="posX" /></div>

			<div class="form-label"> y : </div>
			<div class="form-input"><input type="number" required name="posY" /></div>

			<div class="form-label">&nbsp;</div>
			<div class="form-input"><button>Envoyer</button></div>

			<br>
			<div class="form-label"><a href="">Consulter</a></div>
			<div class="form-label"><a href="">Supprimer</a></div>
		</div>

		<div id ="thePosts">

		<?php

			foreach ($listPostIt as $value) {
				$style = "left:" . $value[1] . "px; top:" . $value[2] . "px;"
				?>
					<div class="postIt" style=<?= $style ?>><?= $value[0] ?></div>
				<?php
			}
		?>

		</div>

	</form>

<?php
	require_once("partial/footer.php");