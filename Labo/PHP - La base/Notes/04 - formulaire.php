<?php
	// "empty()" Permet de vérifier que:
	// - variable existe et
	// - qu'elle n'est pas nulle, vide, false, 0, ni ""

	$showBox = false;
	if (!empty($_GET["info"])){
		//info est vide par défaut, va lancer une erreur (quand on lance pour la première fois)
		//echo $_GET["info"]; //$_POST["info"]
		$showBox = true;
	}
?>



<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Les formulaires</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<?php
			if($showBox){
				?>
				<div style="color:green">Yéé!! <?= $_GET["info"] ?></div>
				<?php
			}
		?>

		<!-- get pas sécuritaire, on peut regarder dans l'historique les informations qui ont été rentrée dans l'url -->
		<form action="04 - formulaire.php" method="get">
			<div>
				Info : <input type="text" required name="info" />
			</div>
			<div>
				<input type="submit" value="Go!" />
			</div>
		</form>
	</body>
</html>
