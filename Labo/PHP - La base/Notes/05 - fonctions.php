<?php
	function hasher($mot, $salt = "qwerty"){

		$val = sha1($mot . $salt); //md5() & sha1() --> pas sécuritaire pour mdp

		return $val;
	}
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Les fonctions</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<h1>Création d'un hash</h1>
		<?= hasher("Gabriel") ?>
		<br>
		<?= hasher("Gabriel", "De Carufel") ?>
	</body>
</html>
