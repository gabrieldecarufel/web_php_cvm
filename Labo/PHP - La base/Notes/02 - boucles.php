<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Boucles</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<h1>Champs radio</h1>

		<?php
			/*
				ne pas faire cette méthode (malgré quelle soit fontionnelle)
				difficile à entretenir
			for ($i=0; $i < 50; $i++ ) {


				echo "<div>";
				echo "<input type='radio'>$i";
				echo "</div>";
			}*/
			for ($i=0; $i < 50; $i++ ) {
				?>
				<div>
					<label>
						<input type='radio' name="age"><?= $i ?>
					</label>
				</div>
				<?php
			}

			$i = 10;
			while ($i > 0){
				$i--;
			}
		?>

	</body>
</html>
