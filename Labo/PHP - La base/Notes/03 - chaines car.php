<?php
	$mot = "Lorem ipsum sin dolori";
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Les chaînes</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<h1>Mot : <?= $mot ?> </h1>
		<!-- fonction pour savoir longueur de la string -->
		<div>Longueur : <?= strlen($mot) ?> </div>
		<!-- on commence l'occurence -->
		<div>Position orem : <?= strpos($mot, "orem") ?> </div>
		<!-- valeur qu'on cherche, valeur avec lequel on remplace, la variable -->
		<div>Remplacer : <?= str_replace("orem", "gique", $mot) ?> </div>
		<!-- ou on commence, jusqu'a ou (retourne orem) -->
		<div>Substring : <?= substr($mot, 1, 4) ?> </div>
	</body>
</html>
