<?php
	$showBox = false;
	if (!empty($_GET["searchBox"])){
		$result = strlen($_GET["searchBox"]);
		$showBox = true;
	}
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>String stats</title>
        <meta charset="utf-8" />
		<link href="css/global.css" rel="stylesheet" />
	</head>
	<body>
		<form action="index.php" method="get">
			<div>
				<div id="analyze-icon">

				</div>
				<div id="analyze-result">
					Entrez une chaîne de caractères et appuyez sur analyser!
				</div>
				<div id="search-line">
					<div>
						<input type="text" required name="searchBox" placeholder="Texte à analyser" />
					</div>
					<div class="btn">
						<button>Analyser</button>
					</div>
				</div>
				<?php
					if($showBox){
				?>
						<div style="color:green">longueur de la chaîne
							<?= $_GET["searchBox"]?> :
							<?= $result ?>
						</div>
				<?php
					}
				?>
			</div>
		</form>
	</body>
</html>