<?php

	require_once("action/AdminIndexAction.php");

	$action = new AdminIndexAction();
	$action->execute();

	require_once("partial/header.php");
?>

	<form name="formModification" action="admin-index.php" method="get" onsubmit="setText()">

		<textarea name="data" id="data" cols="100" rows="40"><?= $action->getText(); ?></textarea>

		<div class="form-input">
			<button type="submit" >Enregistrer</button>
		</div>

	</form>

<?php
	require_once("partial/footer.php");