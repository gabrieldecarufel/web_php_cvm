<?php
	require_once("action/indexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<div id="page-index">
		<img src="images/water-drop.png" alt= ""/>

		<?php
			echo $action->text;
		?>

	</div>
<?php
	require_once("partial/footer.php");