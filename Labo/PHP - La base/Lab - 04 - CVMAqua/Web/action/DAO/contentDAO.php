<?php

	function getContent($file) {
		return file_get_contents($file, "r");
	}

	function setContent($file, $data) {
		file_put_contents($file, $data);
	}