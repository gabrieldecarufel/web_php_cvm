<?php
	require_once("action/CommonAction.php");

	require_once("action/DAO/contentDAO.php");

	class IndexAction extends CommonAction {

		public static $FILE = "index.txt";

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
			$this->text = null;
		}

		protected function executeAction() {
			$this->text = getContent(IndexAction::$FILE);
		}
	}