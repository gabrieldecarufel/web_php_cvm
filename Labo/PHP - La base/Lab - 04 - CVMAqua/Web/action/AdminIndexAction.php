<?php
	require_once("action/DAO/contentDAO.php");
	require_once("action/CommonAction.php");
	require_once("action/indexAction.php");

	class AdminIndexAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
			$this->data = getContent(IndexAction::$FILE);
		}

		protected function executeAction() {
			if (!empty($_GET["data"]))
			{
				setContent(IndexAction::$FILE, $_GET["data"]);
			}
		}

		public function getText(){
			return getContent(IndexAction::$FILE);
		}

	}