
//new array (la liste ne sera redéfini, donc const. On pourra ajouter/enlever des éléments)
const spriteList = [];
const BALL_COUNT = 20;

window.onload = () => {

	for (i = 0; i< BALL_COUNT; ++i){
		spriteList.push(new Ball(i));
	}

	tick();
}

const tick = () => {
	for (let i = 0; i < spriteList.length; ++i) {
		const sprite = spriteList[i];

		sprite.tick();
	}


	window.requestAnimationFrame(tick);
}
