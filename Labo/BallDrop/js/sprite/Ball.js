class Ball {
	constructor(id) {
		this.node = document.getElementById("ball_" + id);
		this.node.style.left = Math.random() * 400 + "px";

		this.currentTop = 10 + Math.random() *40;
		this.node.style.top = this.currentTop + "px";
		this.speedY = 2;
		this.velocityY = 0.5; //AccelerationY
	}

	tick(){
		this.speedY += this.velocityY;
		this.currentTop += this.speedY;

		if(this.currentTop > 220) {
			this.currentTop = 220;
			this.speedY = -this.speedY/1.5;
		}

		this.node.style.top = this.currentTop + "px";
	}

}
