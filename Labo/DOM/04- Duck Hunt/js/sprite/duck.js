class Duck{
	constructor(parent){

		console.log("créé");
		this.gauche = (Math.random()  < 0.5);//retourne un boolean
		this.dim= 60;
								//if, else
		this.posX = (this.gauche) ? 0 : 670 -this.dim;

		this.posY = 10 + Math.random() * 300;
		this.speed = 1 + 2 * Math.random();
		this.node = document.createElement("div");
		this.node.setAttribute("class", "duck duck_1");
		if (!this.gauche)
		{
			this.node.setAttribute("style", "-webkit-transform: scaleX(-1); transform: scaleX(-1);")
		}
		this.parent=parent;
		this.state = 1;

		this.node.style.top = this.posY + "px";
		this.node.style.left = this.posX + "px";
		this.parent.appendChild(this.node);
	}

	tick() {
		let alive = true;

		this.state = (this.state >= 100) ? 1 : this.state+1;

		if (this.state % 25 == 0)
		{
			this.node.setAttribute("class", "duck duck_1");
		}
		else  //(this.state == 2)
		{
			this.node.setAttribute("class", "duck duck_2");
		}
		/*
		else if (this.state == 3) {
			this.node.setAttribute("class", "duck duck_3");
		}*/

		if (this.gauche) {
			this.posX+=this.speed;
		}
		else{
			this.posX-=this.speed;
		}

		this.node.style.left = this.posX + "px";

		// si il demarre à droite et depasse à gauche
		if (this.posX < 0 && !this.gauche)
		{
			alive = false;
			this.node.remove();
		}
		// si il demarre à gauche et depasse à droite
		else if(this.posX > 670-this.dim && this.gauche){
			alive = false;
			this.node.remove();
		}

		return alive;
	}
}