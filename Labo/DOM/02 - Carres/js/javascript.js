let parent = null;
let spriteList = [];

document.onmousemove = (e) => {
	console.log("hello")
	spriteList.push(new Carre(e.offsetX, e.offsetY));
}

window.onload = () => {
	tick();
}

const tick = () => {

	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		let alive = sprite.tick();
		if (!alive){
			spriteList.splice(i,1);
			i--;
		}
	}
	window.requestAnimationFrame(tick);
}

class Carre{
	constructor(x,y){
		this.posX = x;
		this.posY = y;
		this.speed = 0;

		this.node = document.createElement("div");
		//this.node.style.backgroundColor = "#fff";
		this.node.setAttribute("class", "square");

		this.node.style.top = this.posY + "px";
		this.node.style.left = this.posX + "px";
		document.body.appendChild(this.node);
	}

	tick() {
		let alive = true;
		this.posY -= this.speed;
		this.speed += 0.1;

		this.node.style.top = this.posY + "px";
		if (this.posY < 0)
		{
			alive = false;
			this.node.remove();
		}

		return alive;
	}
}