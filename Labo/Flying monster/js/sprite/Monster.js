class Monster {
	constructor(id) {
		this.node = document.getElementById("monster_" + id);

		this.posY = (50 + Math.random() * 100);
		this.node.style.top = this.posY + "px";

		this.posX = 10+ Math.random() * 1200;
		this.node.style.left = this.posX + "px";

		this.speedMax = 8;
		this.speedX = 0.1;
		this.speedY = 0.1;
		this.velocity = 0.1; //AccelerationY
		this.angle = 0;
	}


	tick(){
		this.angle =  getElementAngle(this.posX, this.posY,mousePosX,mousePosY)
		rotateElement(this.node, this.angle);

		this.posX += this.speedX;
		this.posY += this.speedY;

		if (this.posX < mousePosX){
			if (this.speedX < this.speedMax){
				this.speedX += this.velocity;
			}
		}
		else {
			if (this.speedX > -this.speedMax){
				this.speedX -= this.velocity;
			}
		}

		if (this.posY < mousePosY){
			if (this.speedY < this.speedMax){
				this.speedY += this.velocity;
			}
		}
		else{
			if (this.speedY > -this.speedMax){
				this.speedY -= this.velocity;
			}
		}


		this.node.style.top = this.posY + "px";
		this.node.style.left = this.posX + "px";
	}
}