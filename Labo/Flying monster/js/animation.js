var mousePosX= 0;
var mousePosY = 0;
const spriteList = [];
const MONSTER_COUNT = 20;

window.onload = function() {
	document.getElementById("main").onmousemove = function (event) {
		mousePosX = event.pageX;
		mousePosY = event.pageY;
		console.log(mousePosX + "," + mousePosY);
	}
	for (i = 0; i< MONSTER_COUNT; ++i){
		spriteList.push(new Monster(i));
	}
	tick();
}


const tick = () => {
	for (let i = 0; i < spriteList.length; ++i) {
		const sprite = spriteList[i];
		sprite.tick();
	}


	window.requestAnimationFrame(tick);
}