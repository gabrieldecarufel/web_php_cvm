
window.onload = () => {
		document.getElementById("analyze-btn").onclick = process;
}

const process = () => {
	document.getElementById("analyze-icon").style.display = "block";

	setTimeout( () => {
		document.getElementById("analyze-icon").style.display = "none";
		document.getElementById("analyze-result").style.display = "block";
	}, 2000)
}