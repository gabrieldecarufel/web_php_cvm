const spriteList = [];
let ctx = null;
let canvas = null;


window.onload = () =>{
	ctx = document.querySelector("#canvas").getContext("2d");

	let nodeX = document.getElementById("x");
	let nodeY = document.getElementById("y");
	let nodeSize = document.getElementById("size");

	document.getElementById("btn-add").onclick = () =>{
		console.log("button is clicked");
		spriteList.push(new Square(nodeX.value, nodeY.value, nodeSize.value, Math.random() *255, Math.random() *255, Math.random() *255));
		tick();
	}

	//onmousemove
	document.querySelector("#canvas").onmousemove = function (e) {
		let posX = e.offsetX;
		let posY = e.offsetY;

		for (i = 0; i< spriteList.length; ++i){
			const sprite = spriteList[i];
			if (sprite.overMe(posX, posY) == true){
				sprite.tick();
			}
		}
	}
}


const drawRect = (x, y, width, height, c1, c2, c3) => {
	ctx.fillStyle = "rgba("+ c1 +", "+ c2 +", "+ c3 +", 1)";
	ctx.fillRect(x, y, width, height);
	console.log("dessin", x,y,width, height);
}

tick = () => {
	ctx.clearRect(0,0,650,650);
	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		drawRect(sprite.posX, sprite.posY,sprite.size, sprite.size, sprite.color1, sprite.color2, sprite.color3);
	}
	window.requestAnimationFrame(tick);
}