class Square {

	constructor(posX, posY, size, color1, color2, color3) {
		this.posX = posX;
		this.posY = posY;
		this.size = size;
		this.color1 = color1;
		this.color2 = color2;
		this.color3 = color3;
	}

	overMe(posX, posY) {
		let succeed = false;
		if (this.posX < posX && this.posX +this.size > posX){
			if (this.posY < posY && this.posY +this.size > posY){
				succeed = true;
			}
		}
		return succeed;
	}


	tick() {
		this.color1 = Math.random() *255;
		this.color2 = Math.random() *255;
		this.color3 = Math.random() *255;
	}
}