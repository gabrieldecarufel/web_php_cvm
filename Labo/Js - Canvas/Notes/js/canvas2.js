
let compteurRect = 0;
let ctx = null;
let canvas = null;
const spriteList = [];


const width = 40;
const height = 40;

window.onload = () => {

	ctx = document.querySelector("#canvas").getContext("2d");

	//méthode 1
	document.querySelector("#canvas").onclick = function (e) {
		let opacity = 1;
		let posX = e.offsetX - width/2
		let posY = e.offsetY - height/2
		spriteList.push(new rect2(posX,posY,width,height,opacity));
		drawRect(posX, posY, width, height, opacity)
	}
	tick();
}

const drawRect = (x, y, width, height, opacity) => {
	ctx.fillStyle = "rgba(225, 0, 0, " + opacity+")";
	ctx.fillRect(x, y, width, height);
}

const tick = () => {
	ctx.clearRect(0,0,650,650);
	for (i = compteurRect; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		sprite.tick();
		drawRect(sprite.posX, sprite.posY, sprite.width, sprite.height, sprite.opacity)
	}
	window.requestAnimationFrame(tick);
}


//méthode 2
//document.onclick = (e) => {
//	let posX = e.offsetX - width/2
//	let posY = e.offsetY - height/2
//	drawRect(posX, posY, width, height)
//}