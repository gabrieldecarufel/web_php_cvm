console.log("javascript lié");

	const validateL = () => {
		let success = true;

		let node =  document.getElementById("prenom");
		console.log(node.value);
		success = validateName(node, success);
		console.log("success :" ,success);

		node =  document.getElementById("nom");
		console.log(node.value);
		success = validateName(node, success);
		console.log("success :" ,success);


		node = document.getElementById("email");
		console.log(node.value);
		success = validateEmail(node,success);
		console.log("success :" ,success);

		node = document.getElementById("password");
		console.log(node.value);
		success = validatePass(node,success);
		console.log("success :" ,success);

		return success;
	}

	const validateName = (node,success) => {
		if(node.value.length < 2) {
			success = false;
		}

		if(node.value.length > 20) {
			success = false;
		}

		const pattern = /^[a-zA-Z]*$/;
		const strToCompare = node.value;

		if (strToCompare.match(pattern) == null){
			success = false;
		}

		return success;
	}

	const validateEmail = (node, success) => {
		//MiniHD@dwag.com
		const pattern = /^[a-zA-Z]+@[a-zA-Z]+\.[a-zA-Z]{2,}$/;
		const strToCompare = node.value;
		if (strToCompare.match(pattern) == null){
			success = false;
		}
		return success
	}

	const validatePass = (node, success) => {
		if(node.value.length < 6) {
			success = false;
		}
		return success;
	}
