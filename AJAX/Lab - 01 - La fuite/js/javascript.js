window.onload = () => {
	let value = 0;

	document.querySelector(".arrow-up").onclick = () => {
		value = 0;
	}
	document.querySelector(".arrow-down").onclick = () => {
		value = 2;
	}
	document.querySelector(".arrow-right").onclick = () => {
		value = 3;
	}
	document.querySelector(".arrow-left").onclick = () => {
		value = 1;
	}

	document.querySelector(".controls").onclick = () => {
		$.ajax({
			type : "POST",
			url : "ajax-index.php",
			data : {
				direction : value
			}
		})
	}
}