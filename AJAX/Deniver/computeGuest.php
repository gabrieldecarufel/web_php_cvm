<?php
	require_once("action/ComputeGuestAction.php");

	$action = new ComputeGuestAction();
	$action->execute();

	echo json_encode($action->result);