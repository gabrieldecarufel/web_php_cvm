<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();
?>
<!DOCTYPE html>
<html>
	<head>
			<title>Mon engin de courriels</title>
			<meta charset="utf-8">
			<link rel="stylesheet" href="css/global.css">
			<script src="js/jquery.min.js"></script>
			<script>
				function checkEmails() {
					$.ajax({
						type : "POST",
						url : "ajax.php",
						data : {
							type : "fetch-emails",
							username : "ken",
							password : "AAAaaa111"
						}
					})
					.done(data => {
						data = JSON.parse(data);

						document.querySelector("#contenantCourriels").innerHTML = data
					})
				}
			</script>
	</head>
	<body>
		<div class="container">
			<h1>Mes courriels</h1>

			<p>Vous avez actuellement</p>
			<div id="contenantCourriels">--</div>
			<p>nouveaux courriels.</p>

			<div class="refresh-section">
				<button onclick="checkEmails()">
					<img src="images/refresh.png" alt="Rafraîchir">
				</button>
			</div>
		</div>
	</body>
</html>
