window.onload = () => {

	document.querySelector("#texte").onkeyup = () => {
		$.ajax({
			type : "POST",
			url : "compute-sha1.php",
			data : {
				txt : document.querySelector("#texte").value
			}
		})
		.done(data => {
			data = JSON.parse(data);

			let node = document.createElement("div");
			let nodeTxt = document.createTextNode(data);
			node.appendChild(nodeTxt);

			document.querySelector("#result").appendChild(node);
		})

	}

}