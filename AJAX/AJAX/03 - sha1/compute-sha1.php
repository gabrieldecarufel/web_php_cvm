<?php
	require_once("action/ComputeSha1Action.php");

	$action = new ComputeSha1Action();
	$action->execute();

	echo json_encode($action->result);