<?php
	require_once("action/CommonAction.php");

	class ComputeSha1Action extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->result = "ERROR";

			if (!empty($_POST["txt"])) {
				$this->result = sha1($_POST["txt"]);
			}
		}
	}