
let parent= null;
let result= null;

window.onload = () => {
	parent = document.querySelector("#memo-list");
	result = document.querySelector("#memo-input");

	result.onkeyup = (e) =>{
		//créer le nouvel Élément li
		if (e.which == 13)
		{
			let nouvelElement = document.createElement("div");
			nouvelElement.setAttribute("class", "memo-entry");

			//pour delete
			//nouvelElement.setAttribute("onclick", "enleverNoeud(this)")

			nouvelElement.onclick = () => {
				nouvelElement.remove();
			}

			let textNode = document.createTextNode(result.value);

			//accrocher le texte au li
			nouvelElement.appendChild(textNode);

			//accrocher le li au ul
			parent.appendChild(nouvelElement);
			result.value = "";
		}
	}
}

function enleverNoeud(element) {
	element.remove();
}
