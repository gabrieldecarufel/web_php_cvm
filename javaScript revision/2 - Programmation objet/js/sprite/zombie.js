class Zombie{
	constructor(self){

		//this.width = width;
		this.node=self;
		this.height = this.node.clientHeight;
		this.width = this.node.clientWidth;
		this.maxX=BOX_WIDTH-this.width;
		this.posX=Math.random()*(this.maxX);




		this.node.style.left = this.posX + "px";
		this.speed = 3;
		this.gauche = (Math.random()  < 0.5);

		this.node.style.top = BOX_HEIGHT-this.height+"px";
		console.log(this.node.style.top);

	}

	tick() {
		if ((this.posX <= 0 && !this.gauche) || (this.posX >= this.maxX && this.gauche))
		{
			this.gauche= !this.gauche;
		}

		if (this.gauche) {

			this.posX+=this.speed;
		}
		else{

			this.posX-=this.speed;
		}

		this.node.style.left = this.posX + "px";

	}
}