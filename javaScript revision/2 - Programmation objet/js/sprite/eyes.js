class Eyes{
	constructor(parent,x,y){

		this.node = document.createElement("div");
		this.node.setAttribute("class", "eyes");

		this.parent=parent;
		this.node.style.top = y + "px";
		this.node.style.left = x + "px";
		this.parent.appendChild(this.node);
		this.opacity=1
		this.dimSpeed=.01;


	}

	tick() {
		let alive= true;
		this.opacity= this.opacity-this.dimSpeed
		this.node.style.opacity = this.opacity;
		console.log(this.opacity);
		if(this.opacity<=0){
			this.node.remove();
			alive=false;
		}

		return alive
	}
}