let spriteList = [];

let BOX_WIDTH=null;
let BOX_HEIGHT=null;

window.onload= () => {
	BOX_HEIGHT= document.getElementById("zombie-container").clientHeight;
	BOX_WIDTH= document.getElementById("zombie-container").clientWidth;
	zombie1=new Zombie(document.getElementById("zombie1"));
	zombie2=new Zombie(document.getElementById("zombie2"));
	tick();

	document.getElementById("zombie-container").onclick = e =>{


		spriteList.push(new Eyes(document.getElementById("zombie-container"),e.offsetX,e.offsetY));

}
}



const tick = () => {

		zombie1.tick();
		zombie2.tick();
		for (i = 0; i< spriteList.length; ++i){
			const sprite = spriteList[i];
			let alive = sprite.tick();
			if (!alive){
				spriteList.splice(i,1);
				i--;
			}
		}



		window.requestAnimationFrame(tick);
}