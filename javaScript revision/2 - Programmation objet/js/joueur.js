class Joueur {
	constructor(parent){
		this.posX = 200;
		this.posY = 200;
		this.speed = 0;

		this.dim = 75;

		this.score = 0;
		this.vie = 3;

		this.parent = parent;

		this.node = document.createElement("div");
		//this.node.style.backgroundColor = "#fff";
		this.node.setAttribute("class", "gun");

		this.node.style.top = this.posY + "px";
		this.node.style.left = this.posX + "px";

		this.parent.appendChild(this.node);
	}

	tick(x,y) {
		console.log(this.parent.style.marginLeft);
		this.posX = x - this.dim -200;
		this.posY = y - this.dim;

		// console.log("x", x, "y", y);

		this.node.style.top = this.posY + "px";
		this.node.style.left = this.posX + "px";
	}
}