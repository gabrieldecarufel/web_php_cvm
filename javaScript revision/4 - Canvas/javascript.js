let spriteList = [];
let ctx = null;
let canvas= null;
let BOX_WIDTH = 400;
let BOX_HEIGHT = 300;
let ninja= null;


window.onload= () => {

	ctx = document.getElementById("canvas").getContext("2d");

	ninja = new NinjaGaden();

	tick();

}

const tick = () => {
	ctx.clearRect(0, 0, BOX_WIDTH, BOX_HEIGHT);
	ninja.tick();

	if(spacePushed){
		spriteList.push(new NinjaStar(ninja.posX, ninja.posY));
		spacePushed = !spacePushed;
	}
	for (i = 0; i< spriteList.length; ++i){
		let alive = spriteList[i].tick();
		if (!alive){
			spriteList.splice(i,1);
			i--;
		}

	}


	window.requestAnimationFrame(tick);

}