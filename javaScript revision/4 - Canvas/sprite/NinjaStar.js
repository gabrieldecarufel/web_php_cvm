class NinjaStar{
	constructor(posX,posY){
		this.posX = posX;
		this.posY = posY+20;
		this.speed =5;

		this.star = new Image();
		this.star.onload = () => {
		ctx.drawImage(this.star, this.posX, this.posY);
	}
	this.star.src = "images/star.png";

	}



	tick(){
		let alive = true;
		ctx.drawImage(this.star, this.posX, this.posY)

		this.posX += this.speed;
		if(this.posX >= 350){
			alive = false;
		}
		return alive;
	}
}