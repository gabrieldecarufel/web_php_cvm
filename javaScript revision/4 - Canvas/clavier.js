var rightPushed = false;
var leftPushed = false;
var spacePushed = false;

document.onkeydown = function (e) {
    if (e.which == 65)         leftPushed = true; //a
    else if (e.which == 68) rightPushed = true; //d
    else if (e.which == 32) spacePushed = true; //space
}

document.onkeyup = function (e) {
    if (e.which == 65)         leftPushed = false;
    else if (e.which == 68) rightPushed = false;
    else if (e.which == 32) spacePushed = false;
}