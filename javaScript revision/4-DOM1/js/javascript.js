let parent= null;
let result= null;
let text= null;
let compteur = 0;

window.onload = () => {
	parent = document.querySelector(".main-content");
	text = "ceci est un nouveau paragraphe";
	select = document.querySelector("select");

	button = document.querySelector("button");

	button.onclick = () =>{
		result = select.options[select.selectedIndex].value;

		for (let i = 0; i < result; ++i)
		{
			compteur++;
			//créer le nouvel Élément li
			let nouvelElement = document.createElement("div");
			nouvelElement.setAttribute("class", "para");

			//pour delete
			//nouvelElement.setAttribute("onclick", "enleverNoeud(this)")

			nouvelElement.onclick = () => {
				nouvelElement.remove();
				compteur--;
			}

			let textNode = document.createTextNode(text + " " + compteur);

			//accrocher le texte au li
			nouvelElement.appendChild(textNode);

			//accrocher le li au ul
			parent.appendChild(nouvelElement);
		}

		text.value = "";
	}
}

function enleverNoeud(element) {
	element.remove();
}
