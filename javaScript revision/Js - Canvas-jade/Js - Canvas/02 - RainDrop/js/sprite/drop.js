
class Drop {
	constructor(){
		this.posX = Math.random() * 500;
		this.posY = -10;
		this.size = 5+Math.random() * 10;
		this.speed = 20;
	}

	tick() {
		this.posY += this.speed * (this.size/100);

		ctx.fillStyle = "blue";
		ctx.fillRect(this.posX, this.posY, this.size, this.size)

		let alive = this.posY < 300;

		return alive;
	}
}