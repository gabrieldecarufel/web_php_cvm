const spriteList = [];
let ctx = null;
let background = null;
let startDrop = 20;

window.onload = () => {

	ctx = document.querySelector("#canvas").getContext("2d");

	background = new Image();
	background.onload = () => {
		ctx.drawImage(background, 0, 0);
	}
	background.src = "images/landscape.png";

	tick();
}


tick = () => {
	ctx.clearRect(0,0,500,300);
	ctx.drawImage(background, 0, 0);
	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		let alive = sprite.tick();

		if (!alive){
			spriteList.splice(i,1);
			i--;
		}
	}

	if (Math.random()<0.2 )
	{
		spriteList.push(new Drop());
	}

	window.requestAnimationFrame(tick);
}