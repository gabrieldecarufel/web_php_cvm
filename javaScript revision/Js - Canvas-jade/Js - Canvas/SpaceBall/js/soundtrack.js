let audio = null;
const playMe = () => {
	audio = new Audio("music/SbClosingTheme.mp3");

	//on loop
	audio.addEventListener("ended", function(){
		audio.currentTime = 0;
		console.log("ended");
		audio.play();
   });

	audio.play();
}

const stopMe = () => {
	audio.currentTime = 0;
	audio.pause();
}
