
class Bob {
	constructor(){
		this.columnCount = 12;
		this.rowCount = 5;
		this.refreshDelay = 100; 			// msec
		this.loopColumns = true; 			// or by row?
		this.scale = 2.0;
		this.tiledImage = new TiledImage("images/sb-spritesheet.png", this.columnCount, this.rowCount, this.refreshDelay, this.loopColumns, this.scale, null);

		this.tiledImage.changeRow(2);				// One row per animation
		this.tiledImage.changeMinMaxInterval(1, 10); 	// Loop from which column to which column?

		this.ground = 360;
		this.posX = 220;
		this.dim = 32.5 * this.scale;
		this.posY = this.ground;
		this.speed = 2;

		this.isJumping = false;
		this.isOnGround = true;
		this.onMove = false;
		this.alive = true;
	}

	tick() {
		if (rightPushed){
			this.posX +=this.speed;
		}
		if (leftPushed){
			this.posX -=this.speed;
		}
		if (spacePushed){
			if (this.posY > 0)
			{
				this.posY-=this.speed;
			}
			this.isJumping = !this.isJumping;
			this.isOnGround = false;
			this.onMove = true;
		}
		else {
			if (this.ground > this.posY){
				this.posY+=this.speed;
				this.isOnGround = false;
				this.onMove = true;
			}
			else if (this.ground == this.posY)
			{
				if (this.onMove)
				{
					this.isOnGround = true;
					this.onMove = false;
				}
			}
		}

		if (this.isJumping)
		{
			console.log("I'm Jumping!")
			this.tiledImage.changeRow(3);				// One row per animation
			this.tiledImage.changeMinMaxInterval(3, 8); 	// Loop from which column to which column?
			this.isJumping = !this.isJumping;
		}
		if (this.isOnGround)
		{
			console.log("I'm on Ground!")
			this.tiledImage.changeRow(2);				// One row per animation
			this.tiledImage.changeMinMaxInterval(1, 10); 	// Loop from which column to which column?
			this.isOnGround = false;
		}

		ctx.fillStyle = "blue";
		ctx.fillRect(this.posX-this.dim/2, this.posY, this.dim, this.dim)

		this.tiledImage.tick(this.posX, this.posY, ctx);
	}

}