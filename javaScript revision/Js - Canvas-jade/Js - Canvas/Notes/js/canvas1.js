let ctx = null; //context

window.onload = () => {
	//ctx = document.getElementById("canvas").getContext("2d");
	//querySelector retourne le premier qu'il trouve
	ctx = document.querySelector("#canvas").getContext("2d");

	//retourne tout les occurences de div
	//let tableau = document.querySelectorAll("div");

	drawRect(10, 10, 40, 40);
}

const drawRect = (x, y, width, height) => {
	//quelle couleur tu veux remplir ton carré
	ctx.fillStyle = "rgba(0, 0, 0, 0.4)";
	ctx.fillRect(x, y, width, height);

	//couleur de contour
	ctx.strokeStyle = "black";
	ctx.strokeRect(x, y, width, height);

}

document.onkeyup = (e) => {
	if (e.which == 32){
		//passe l'efface dans ces dimensions
		ctx.clearRect(0,0,650,650);
		//alert("Aye sir!");
	}

}