let ctx = null;

window.onload = () => {

	ctx = document.querySelector("#canvas").getContext("2d");

	//Écrire du texte

	ctx.font = "24px Arial";
	ctx.Style = "green";
	ctx.fillText("Hello world", 5 , 200);

	//afficher une image
	let doge = new Image();
	doge.onload = () => {
		ctx.drawImage(doge, 400, 20);
	}

	//appeler cette ligne va appeler doge.onload en même temps
	doge.src = "images/doge.png";


	//ctx.drawImage(doge, 40, 10);

	//setTimeout(() => {
	//	if(doge.complete){
	//		ctx.drawImage(doge, 40, 10);
	//	}
	//} ,1000);


	//dégradé linéraire
	//le dégradé s'effectue dans l'angle entre 10,10 et 150,150
	//a 20% == bleu/mauve
	//a 80% == vert
	//a 100% == bleu foncé
	let degrade = ctx.createLinearGradient(10, 10, 150, 150);
	degrade.addColorStop(0.2, "#66f");
	degrade.addColorStop(0.8, "#2f2");
	degrade.addColorStop(1.0, "#00f");
	ctx.fillStyle = degrade;
	ctx.fillRect(10, 10, 150, 150);


	//
	const posX = 300;
	const posY = 300;
	const radius = 150;

	degrade = ctx.createRadialGradient(posX, posY, 5, posX, posY, radius);
	degrade.addColorStop(0, "#600");
	degrade.addColorStop(0.9, "rgba(255,255,255,1)");
	degrade.addColorStop(1, "rgba(255,0,0,0)");
	ctx.fillStyle = degrade;
	ctx.fillRect(posX- radius, posY -radius, radius *2, radius *2);

}