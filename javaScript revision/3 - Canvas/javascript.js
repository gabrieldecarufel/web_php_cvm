let spriteList = [];
let ctx = null;
let canvas= null;
let BOX_WIDTH;
let BOX_HEIGHT;
let ninja= null;
window.onload= () => {
	BOX_WIDTH=400;
	BOX_HEIGHT=300;
	canvas = document.querySelector("canvas")
	ctx =canvas.getContext("2d");
	ninja =new NinjaGaden();
	tick();

}

document.onkeydown = (e) => {

	console.log(e.which);
	if(e.which==68){
		ninja.moveRight();
	}
	else if(e.which==65){
		ninja.moveLeft();
	}
	else if(e.which==32){
		ninja.posX
		spriteList.push(new NinjaStar(ninja.posX,250,100));
	}
}

const tick = () => {

	ctx.clearRect(0,0,650,650);
	ninja.tick();

	for (let i = 0; i < spriteList.length; i++) {
		const sprite =spriteList[i]

		let alive= sprite.tick();

		if(!alive) {
			spriteList.splice(i,1);
			i--;
		}
		 }
		window.requestAnimationFrame(tick);
}