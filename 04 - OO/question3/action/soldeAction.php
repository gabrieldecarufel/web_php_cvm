<?php 

	/*
	 * Fonction permettant de retourner la liste des 5 dernières transactions
	 * financières de la compagnie
	 * 
	 * Cette fonction retourne un tableau de transactions. 
	 * 
	 * Une transaction est un tableau contenant 3 informations : 
	 *    ["type"] = type de transaction, Achat ou vente
	 *    ["montant"] = montant de la transaction (ex: 30)
	 *    ["personne"] = Personne qui a fait la tansaction
	 * 
	 * 
	 * NE PAS MODIFIER CETTE FONCTION
	 */
	function execute() {
		$transactions = array();
		$owners = array("John", "Jack", "James");
		$types = array("Achat", "Vente");
		
		for ($i = 0; $i < 5; $i++) {
			$transactions[] = array("personne" => $owners[rand(0,2)], 
									"type" => $types[rand(0,1)], 
									"montant" => rand(1, 100));
		}
		
		return $transactions;
	}
