<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <title>Solde</title>
        <link href="css/global.css" rel="stylesheet" />   
    </head>
    <body>
		<div class="site-container">
		    <div class="page-container">
		    	<div class="site-header">
		    		<div class="page-title-section">
			    		<h1>Solde du compte de la compagnie CVMandrake</h1>
		    		</div>
		    		<div class="clear"></div>
		    	</div>
		    	<div class="page-content">