<?php 
	require_once("partial/header.php");
?>

<div id="page-solde">
	<div>
		Solde actuel du compte : $ 0
	</div>
	
	<table border="1">
		<tr>	
			<th>Personne</th>
			<th>Type</th>
			<th>Montant</th>
		</tr>
		<!-- 
			Exemple de ligne : 
		<tr>
			<td>col 1</td>
			<td>col 2</td>
			<td>col 3</td>
		</tr> --> 
	</table>
	
	<div>
		Votre solde actuel est de : $ 0
	</div>
</div>

<?php 
	require_once("partial/footer.php");
?>