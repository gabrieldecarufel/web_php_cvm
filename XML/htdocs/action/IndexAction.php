<?php

require_once("action/CommonAction.php");
require_once("action/DAO/RSSDAO.php");
class IndexAction extends CommonAction {

	public $channel;
	public function __construct() {
		parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
	}

	protected function executeAction() {
		if(!empty($_GET["url"])){

			$this->channel =RSSDAO::getElements($_GET["url"]);


	}
}