<?php

	require_once("utils/morse-utils.php");

	function execute(){
		$code=0;
		if(!empty($_GET["champASCII"]))
		{
		$code= encodeMorse($_GET["champASCII"]);
		return $code;
		}
		elseif(!empty($_GET["champMorse"]))
		{
		$code= decodeMorse($_GET["champMorse"]);
		return $code;
		}
		return $code;
	}