<?php
	class Connection {
		private static $connection;

		public static function getConnection() {
			// singleton!
			if (Connection::$connection == null) {
				Connection::$connection = new PDO("oci:dbname=" . DB_NAME, DB_USER, DB_PASS);
				Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}

			return Connection::$connection;
		}

		// Not useful... PHP ferme la connexion automatiquement
		// public static function closeConnection() {
		// 	if (Connection::$connection != null) {
		// 		Connection::$connection = null;
		// 	}
		// }
	}