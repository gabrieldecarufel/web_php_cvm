<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");

	class LoginAction extends CommonAction {
		public $wrongLogin;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->wrongLogin = false;

			if (isset($_POST["username"])) {
				$userInfo = UserDAO::authenticate($_POST["username"], $_POST["pwd"]);

				if (!empty($userInfo)) {
					$_SESSION["username"] = $userInfo["FIRST_NAME"];
					$_SESSION["visibility"] = $userInfo["VISIBILITY"];

					header("location:home.php");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			}
		}
	}
