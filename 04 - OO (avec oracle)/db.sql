CREATE TABLE USERS (
	ID NUMBER GENERATED AS IDENTITY PRIMARY KEY,
	USERNAME VARCHAR2(40) NOT NULL,
	FIRST_NAME VARCHAR2(40) NOT NULL,
	LAST_NAME VARCHAR2(40) NOT NULL,
	PASSWORD VARCHAR2(255) NOT NULL,
	VISIBILITY NUMBER(1) NOT NULL
);

insert into users(username, first_name, last_name, password, visibility) values
('ftheriault', 'Fred', 'Theriault', '$2a$06$wpxjvPG7GoLM970BCzHK/ulcOQjymZyzC6QRCMvCYwSE4ptTZ28mW', 1);

insert into users(username, first_name, last_name, password, visibility) values
('arthax', 'Arthax', 'GhostInTheMachines', '$2a$06$wpxjvPG7GoLM970BCzHK/ulcOQjymZyzC6QRCMvCYwSE4ptTZ28mW', 1);

commit;

CREATE TABLE USERS (
	ID NUMBER GENERATED AS IDENTITY PRIMARY KEY,
	NAME_COMMENT VARCHAR2(40) NOT NULL,
	COMMENT VARCHAR2(400) NOT NULL,
	DATE_POST timestamp not Null
);