
let spriteListe = [];
function ajouter(x,y) {
	//docuemnt.getElementById("liste");

	spriteListe.push(new Carre(x,y));
}
function supprimerNoeud(element) {
	element.remove();

}
window.onload = () => {
document.onmousemove = function (e) {
		ajouter(parseInt(e.pageX),parseInt(e.pageY));
	}
	tick();

}

const tick =  ()=>{
	for (let i = 0; i < spriteListe.length; i++) {
		const sprite =spriteListe[i]

		if(!sprite.tick()){
			spriteListe.splice(i,1,sprite);
		};
	}
	window.requestAnimationFrame(tick);
}

class Carre {
	constructor(posX,posY){
		this.posX=posX;
		this.posY=posY;
		this.speedY=0;
		this.alive=true;
		this.node = document.createElement("div");
		this.node.setAttribute("class","square");
		this.node.style.left=this.posX+"px";
		this.node.style.top=this.posY+"px";
		document.querySelector("body").appendChild(this.node);
	}

	 tick(){
		this.speedY-=.1;
		this.posY+=this.speedY
		this.node.style.top=this.posY+"px";
		this.alive =  this.posY > -50;


		if(!this.alive) {
			this.node.remove();
		}
		return this.alive;
	}

}
