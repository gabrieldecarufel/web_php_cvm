let spriteList = [];

let gameState = 1;

let joueur = null;
document.onmousemove = (e) => {
	if (joueur)
	{
		joueur.tick(e.pageX, e.pageY);
	}
	//spriteList.push(new Carre(e.offsetX, e.offsetY));
}

document.onkeyup = e =>{
	if(e.which == 32 && gameState !=2 ){
		if (gameState !=2) {
			document.getElementById("frame-" + gameState).style.display = "none";

			gameState = (gameState >= 3) ? 1 : gameState+1;
			let parent = document.getElementById("frame-" + gameState) //.style.display = "block";
			parent.style.display = "block";

			//démarrer le jeu a gameState == 2
			if (gameState == 2)
			{
				joueur= new Joueur(parent);
				document.getElementById("life" ).textContent = joueur.vie;
				document.getElementById("score" ).textContent = joueur.score;
				tick();
			}
		}
	}
}

document.onclick = e =>{
	if(joueur){
		killDuck();
	}
}
const killDuck = ()=>{
	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		if((joueur.posX+joueur.dim/2 <= sprite.posX+sprite.dim && joueur.posX+joueur.dim/2 >= sprite.posX) && (joueur.posY+joueur.dim/2 <= sprite.posY+sprite.dim && joueur.posY+joueur.dim/2 >= sprite.posY))
		{
			sprite.node.remove();
		    spriteList.splice(i,1);
			i--;
			joueur.score+=100;
			document.getElementById("score" ).textContent = joueur.score;
		}
	}
}

const tick = () => {
	for (i = 0; i< spriteList.length; ++i){
		const sprite = spriteList[i];
		let alive = sprite.tick();
		if (!alive){
			spriteList.splice(i,1);
			i--;
			joueur.vie--;
			document.getElementById("life" ).textContent = joueur.vie;
		}
	}

	if (Math.random() < 0.02)
	{
		spriteList.push(new Duck(document.getElementById("frame-2" )));
	}

	if (joueur.vie <= 0)
	{
		console.log("gameover")
		document.getElementById("frame-" + gameState).style.display = "none";
		gameState++;
		let parent = document.getElementById("frame-" + gameState) //.style.display = "block";
		parent.style.display = "block";
		for (i = 0; i< spriteList.length; ++i){
			const sprite = spriteList[i];
			sprite.node.remove();
			spriteList.splice(i,1);
			i--;
		}
		joueur.node.remove();
		joueur = null;
	}
	else{

		window.requestAnimationFrame(tick);
	}
}