let ctx = null;
let canvas= null;
const squaresSprite=[]

var mousePosX= 0;
var mousePosY = 0;
window.onload = () => {
	// ctx = document.getElementById("canvas").getContext("2d");

	canvas = document.querySelector("canvas")
	ctx =canvas.getContext("2d");

		document.querySelector("#btn-add").onclick= ()=> {
			squaresSprite.push(new Square(parseInt(document.getElementById("x").value),parseInt(document.getElementById("y").value),parseInt(document.getElementById("size").value)))
		}
	document.getElementById("container").onmousemove = function (event) {
		mousePosX = parseInt(event.offsetX);
		mousePosY = parseInt(event.offsetY);
		//console.log(mousePosX + "," + mousePosY);

	}


 tick();
}


const tick = () => {
	ctx.clearRect(0,0,650,650);
	for (let i = 0; i < squaresSprite.length; i++) {
		const sprite =squaresSprite[i]
		sprite.tick();
	}

	window.requestAnimationFrame(tick);
}