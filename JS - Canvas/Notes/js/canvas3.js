let ctx = null;


window.onload = () =>{
	ctx= document.querySelector("#canvas").getContext("2d");

	//ecrire du texte
	ctx.font = "24px Arial";
	ctx.fillStyle = "grenn";
	ctx.fillText("hello World " , 5 , 200);

	//Afficher une image
	let doge = new Image();

	doge.onload = () => {
		ctx.drawImage(doge,400,10);
	}
	doge.src= "images/doge.png";

	//degrader linear

	let degrade = ctx.createLinearGradient(0,0,150,150);
		degrade.addColorStop(0.2,"#66f");
		degrade.addColorStop(0.8,"#2f2");
		degrade.addColorStop(1,"#00f");
		ctx.fillStyle = degrade;
		ctx.fillRect(0,0,150,150)

	const posX=300;
	const posY = 300;
	const radius = 150

	degrade = ctx.createRadialGradient(posX-30,posY-30,5,posX,posY,radius);
	degrade.addColorStop(0, "#600");
	degrade.addColorStop(0.9,"rgba(255,255,255,1)");
	degrade.addColorStop(1,"rgba(255,0,0,0)");
	ctx.fillStyle=degrade;
	ctx.fillRect(posX-radius,posY-radius,radius*2,radius*2);
}