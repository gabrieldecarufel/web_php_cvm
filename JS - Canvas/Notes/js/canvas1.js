let ctx = null;

window.onload = () => {
	// ctx = document.getElementById("canvas").getContext("2d");
	ctx = document.querySelector("#canvas").getContext("2d");

	drawRect(10,10,40,40)

}

const drawRect= (x,y,width,height) => {
	ctx.fillStyle="rgba(0,0,0,0.4)";
	ctx.fillRect(x,y,width,height);

	ctx.strokeSyle= "black";
	ctx.strokeRect(x,y,width,height);

}

document.onkeyup = (e) => {
	if(e.which==32){
		ctx.clearRect(0,0,650,650);
	}


}