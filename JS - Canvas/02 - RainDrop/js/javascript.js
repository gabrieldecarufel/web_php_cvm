let ctx = null;
let canvas= null;
const rainSprite=[]
let backround = new Image();

window.onload = () => {
	// ctx = document.getElementById("canvas").getContext("2d");

	ctx= document.querySelector("#canvas").getContext("2d");



	backround.onload = () => {
		ctx.drawImage(backround,0,0);
	}
	backround.src= "images/landscape.png";

    tick();
}


const tick = () => {

	ctx.clearRect(0,0,650,650);
	ctx.drawImage(backround,0,0);
	if (Math.random()< 0.05)
	{
		rainSprite.push(new Rain());
	}

	for (let i = 0; i < rainSprite.length; i++) {
		const sprite =rainSprite[i]

		let alive= sprite.tick();

		if(!alive) {
			rainSprite.splice(i,1);
			i--;
		}
	}

	window.requestAnimationFrame(tick);
}

class Rain {
	constructor(){
		this.posX=Math.random()*500;
		this.posY=-10;

		this.size=Math.random()*5 +2;

		this.speed=this.size/5 ;//gravity

	}

	 tick(){

		console.log("here");
		ctx.fillStyle="blue";

		this.posY += this.speed;

		ctx.fillRect(this.posX,this.posY,this.size,this.size) ;

		return this.posY < 310;
	}

}