let ctx = null;
let canvas= null;
const blockSpriteListe=[]
let backround = new Image();
const BALL_SIZE=25;
const BLOCK_SIZE= 50;
const SPEED=5;
const START_X=150;
const BLOCK_TOP=230;
let rightPushed = false;
let leftPushed = false;
let spacePushed = false;
const ball =new Ball();
const DEFAULT_SAFE=5;
window.onload = () => {
	// ctx = document.getElementById("canvas").getContext("2d");

	ctx= document.querySelector("#canvas").getContext("2d");

	backround.onload = () => {
		ctx.drawImage(backround,0,0);
	}
	backround.src= "images/space.jpg";

	for (var i = 0 ; i < 150; i++) {
		if (Math.random() > 0.05) {
			blockSpriteListe.push(new Block( i ));
		}
	}
	document.onkeydown = function (e) {
		if (e.which == 65)         leftPushed = true;
		else if (e.which == 68) rightPushed = true;
		else if (e.which == 32) spacePushed = true;
	}

	document.onkeyup = function (e) {
		if (e.which == 65)         leftPushed = false;
		else if (e.which == 68) rightPushed = false;
		else if (e.which == 32) spacePushed = false;
	}

    tick();
}


const tick = () => {


	ctx.clearRect(0,0,650,650);
	ctx.drawImage(backround,0,0);




   if(alive()){
	for (let i = 0; i < blockSpriteListe.length; i++) {
		const sprite =blockSpriteListe[i]

		sprite.tick();

	}

	ball.tick();
	}else{
		ball.fall();
		for (let i = 0; i < blockSpriteListe.length; i++) {
			const sprite =blockSpriteListe[i]

			sprite.fall();

		}
	}
	window.requestAnimationFrame(tick);
}
const alive=()=>{

	let result=[];
	if (!ball.jump){
		blockSpriteListe.forEach(function(block ){if (block.posX <= START_X+BALL_SIZE && block.posX>= (START_X-BLOCK_SIZE))
		result.push(block) ;});
	return result[0]? true : false
	}
	return true;
}